<?php

class ConnectDB {

	private $db   = $_SERVER["DOCUMENT_ROOT"] . "/0618104038_tugas3_db/db/db_contact.mdb";
	private $opt = array(
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
	    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	);
	
	protected $conn;

	public function __construct() {
		if (!file_exists($this->db)) die("Could not find database file.<br />".$this->db);
		try {
			$this->conn = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$this->db; Uid=''; Pwd='';");
		} catch (PDOException $e) {
		    die("Error!: " . $e->getMessage() . "<br/>");
		}
	}
}